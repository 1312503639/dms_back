package com.dms.system;

import com.dms.common.utils.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

/** @author 25377 */
@SpringBootApplication
@EntityScan("com.dms")
public class DmsSystemApplication {

  public static void main(String[] args) {
    SpringApplication.run(DmsSystemApplication.class, args);
  }

  @Bean
  public IdWorker idWorker() {
    return new IdWorker();
  }
}
