package com.dms.system.dao;

import com.dms.domain.system.entity.PermissionMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *  * 企业数据访问接口
 *
 * @author 25377
 */
public interface PermissionMenuDao
    extends JpaRepository<PermissionMenu, String>, JpaSpecificationExecutor<PermissionMenu> {}
