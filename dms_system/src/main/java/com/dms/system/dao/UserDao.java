package com.dms.system.dao;

import com.dms.domain.system.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Mr.Lgg
 * @date 2020/3/17 15:31
 */
public interface UserDao extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

    public User findByUsername(String username);
}
