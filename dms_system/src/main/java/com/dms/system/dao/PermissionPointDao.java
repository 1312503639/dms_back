package com.dms.system.dao;

import com.dms.domain.system.entity.PermissionPoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *  * 企业数据访问接口
 *
 * @author 25377
 */
public interface PermissionPointDao
    extends JpaRepository<PermissionPoint, String>, JpaSpecificationExecutor<PermissionPoint> {}
