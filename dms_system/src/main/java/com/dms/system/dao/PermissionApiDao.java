package com.dms.system.dao;

import com.dms.domain.system.entity.PermissionApi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *  * 企业数据访问接口
 *
 * @author 25377
 */
public interface PermissionApiDao
    extends JpaRepository<PermissionApi, String>, JpaSpecificationExecutor<PermissionApi> {}
