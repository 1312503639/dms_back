package com.dms.system.controller;

import com.dms.common.entity.PageResult;
import com.dms.common.entity.Result;
import com.dms.common.entity.ResultCode;
import com.dms.common.entity.jwt.JwtInfo;
import com.dms.domain.system.entity.response.ProfileResult;
import com.dms.domain.system.entity.User;
import com.dms.common.utils.JwtTokenUtils;
import com.dms.system.service.UserService;
import com.github.pagehelper.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr.Lgg
 * @date 2020/3/17 16:14
 */
@RestController
@CrossOrigin
@RequestMapping("/sys")
public class UserController {

  /** token过期时间 */
  private int expire = -1;

  @Autowired private UserService userService;

  @PostMapping("/user")
  public Result save(@RequestBody User user) {
    User save = userService.save(user);
    System.out.println(save.getId());
    return new Result(ResultCode.SUCCESS);
  }

  @PutMapping("/user/{id}")
  public Result update(@PathVariable("id") String id, @RequestBody User user) {
    user.setId(id);
    userService.update(user);
    return new Result(ResultCode.SUCCESS);
  }

  @DeleteMapping("/user/{id}")
  public Result deleteById(@PathVariable("id") String id) {
    userService.deleteById(id);
    return new Result(ResultCode.SUCCESS);
  }

  @GetMapping("/user/{id}")
  public Result findById(@PathVariable("id") String id) {
    User user = userService.findById(id);
    return new Result(ResultCode.SUCCESS, user);
  }

  @GetMapping("/user")
  public Result findByPage(int page, int size, @RequestParam Map map) {
    Page<User> pages = userService.findByPage(map, page, size);
    PageResult<User> pageResult =
        new PageResult<User>(pages.getTotalElements(), pages.getContent());
    return new Result(ResultCode.SUCCESS, pageResult);
  }

  @GetMapping("/user/list")
  public Result findAll() {
    return new Result(ResultCode.SUCCESS, userService.findAll());
  }

  @GetMapping("/user/userNameList")
  public Result findUserNameList() {
    return new Result(ResultCode.SUCCESS, userService.findUserNameList());
  }

  @PostMapping("/user/login")
  public Result login(@RequestBody User user) {
    User temp = userService.findByUsernam(user.getUsername());
    if (user == null || !temp.getPassword().equals(user.getPassword())) {
      return new Result(ResultCode.MOBILEORPASSWORDERROR);
    } else {
      JwtInfo jwtInfo = new JwtInfo(temp.getId() + "");
      String token = JwtTokenUtils.generatorToken(jwtInfo, -1);
      HashMap<String, Object> map = new HashMap<>(0);
      map.put("token", token);
      return new Result(ResultCode.LOGINSUCCESS, map);
    }
  }

  @PostMapping("/user/profile")
  public Result profile(HttpServletRequest request) {
    String authorization = request.getHeader("Authorization");
    if (StringUtil.isEmpty(authorization)) {
      return new Result(ResultCode.FAIL);
    }
    String token = authorization.replace("Bearer", "");

    String uid = JwtTokenUtils.getTokenInfo(token).getUid();

    System.out.println(uid);

    User user = userService.findById(uid);

    ProfileResult prof = new ProfileResult(user);

    return new Result(ResultCode.GETPROFILE_SUCCESS, prof);
  }
}
