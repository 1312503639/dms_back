package com.dms.system.controller;

import com.dms.common.controller.BaseController;
import com.dms.common.entity.PageResult;
import com.dms.common.entity.Result;
import com.dms.common.entity.ResultCode;
import com.dms.domain.system.entity.Role;
import com.dms.domain.system.entity.response.RoleResult;
import com.dms.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/** @author 25377 */
@RestController
@CrossOrigin
@RequestMapping("/sys")
public class RoleController extends BaseController {

  @Autowired private RoleService roleService;

  /** 分配权限 */
  @RequestMapping(value = "/role/assignPrem", method = RequestMethod.PUT)
  public Result assignPrem(@RequestBody Map<String, Object> map) {
    // 1.获取被分配的角色的id
    String roleId = (String) map.get("id");
    // 2.获取到权限的id列表
    List<String> permIds = (List<String>) map.get("permIds");
    // 3.调用service完成权限分配
    roleService.assignPerms(roleId, permIds);
    return new Result(ResultCode.SUCCESS);
  }

  /**
   * 添加角色
   *
   * @param role
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/role", method = RequestMethod.POST)
  public Result add(@RequestBody Role role) throws Exception {
    role.setCompanyId(companyId);
    roleService.save(role);
    return Result.SUCCESS();
  }

  /**
   * 更新角色
   *
   * @param id
   * @param role
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/role/{id}", method = RequestMethod.PUT)
  public Result update(@PathVariable(name = "id") String id, @RequestBody Role role)
      throws Exception {
    role.setId(id);
    roleService.update(role);
    return Result.SUCCESS();
  }

  /**
   * 删除角色
   *
   * @param id
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/role/{id}", method = RequestMethod.DELETE)
  public Result delete(@PathVariable(name = "id") String id) throws Exception {
    roleService.delete(id);
    return Result.SUCCESS();
  }

  /** 根据ID获取角色信息 */
  @RequestMapping(value = "/role/{id}", method = RequestMethod.GET)
  public Result findById(@PathVariable(name = "id") String id) throws Exception {
    Role role = roleService.findById(id);
    RoleResult roleResult = new RoleResult(role);
    return new Result(ResultCode.SUCCESS, roleResult);
  }

  /** 分页查询角色 */
  @RequestMapping(value = "/role", method = RequestMethod.GET)
  public Result findByPage(int page, int size, Role role) throws Exception {
    Page<Role> searchPage = roleService.findByPage(companyId, page, size);
    PageResult<Role> pr = new PageResult(searchPage.getTotalElements(), searchPage.getContent());
    return new Result(ResultCode.SUCCESS, pr);
  }

  @RequestMapping(value = "/role/list", method = RequestMethod.GET)
  public Result findAll() throws Exception {
    List<Role> roleList = roleService.findAll();
    return new Result(ResultCode.SUCCESS, roleList);
  }
}
