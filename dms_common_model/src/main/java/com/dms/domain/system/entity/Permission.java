package com.dms.domain.system.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/** @Author Lee_Finn @Date 2020-03-18 12:06:14 @Desc null */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@Entity
@Table(name = "pe_permission")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Permission implements Serializable {

  /** 主键 */
  @Id private String id;
  /** 权限描述 */
  private String description;
  /** 权限名称 */
  private String name;
  /** 权限类型 1为菜单 2为功能 3为API */
  private Integer type;
  /** 主键 */
  private String pid;

  private String code;
  /** 企业可见性 0：不可见，1：可见 */
  private Integer enVisible;

  public Permission(String name, Integer type, String code, String description) {
    this.name = name;
    this.type = type;
    this.code = code;
    this.description = description;
  }
}
