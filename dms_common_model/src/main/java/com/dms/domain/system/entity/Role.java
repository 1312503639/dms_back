package com.dms.domain.system.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/** @Author Lee_Finn @Date 2020-03-18 12:02:05 @Desc null */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@Entity
@Table(name = "pe_role")
public class Role implements Serializable {

  /** 主键ID */
  @Id private String id;
  /** 角色名称 */
  @Column(name = "`name`")
  private String name;
  /** 角色代码 */
  @Column(name = "`key`")
  private String key;
  /** 说明 */
  private String description;
  /** 企业id */
  private String companyId;

  // @JsonIgnoreProperties(value = {"User"})
  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "roles")
  // private Set<User> users = new HashSet<User>(0);

  @JsonIgnore
  @ManyToMany
  @JoinTable(
      name = "pe_role_permission",
      joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id")})
  private Set<Permission> permissions = new HashSet<Permission>(0);
}
