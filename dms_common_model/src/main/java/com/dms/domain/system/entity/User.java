package com.dms.domain.system.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/** @Author Lee_Finn @Date 2020-03-17 16:04:32 @Desc null */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@Entity
@Table(name = "pe_user")
public class User implements Serializable {

  /** ID */
  @Id private String id;
  /** 手机号码 */
  private String mobile;
  /** 用户名称 */
  private String username;
  /** 密码 */
  private String password;

  private String role;
  /** 启用状态 0是禁用，1是启用 */
  private Integer enableState;
  /** 在职状态 1.在职 2.离职 */
  private Integer inServiceStatus;
  /** 企业ID */
  private String companyId;
  /** 创建时间 */
  private Date createTime;

  private String companyName;

  private String departmentName;

  private String roleIds;

  /** 用户与角色 多对多 */
  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JsonIgnore
  @JoinTable(
      name = "pe_user_role",
      joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
  private Set<Role> roles = new HashSet<Role>();
}
