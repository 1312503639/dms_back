package com.dms.domain.system.entity.response;

import com.dms.domain.system.entity.Permission;
import com.dms.domain.system.entity.Role;
import com.dms.domain.system.entity.User;
import lombok.Data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Mr.Lgg
 * @date 2020/3/18 12:14
 */
@Data
public class ProfileResult {
  private String mobile;
  private String username;
  private String company;
  private Map<String, Object> roles = new HashMap<>();

  public ProfileResult(User user) {
    this.mobile = user.getMobile();
    this.username = user.getUsername();
    this.company = user.getCompanyName();
    Set<Role> roles = user.getRoles();
    Set<String> menus = new HashSet<>();
    Set<String> points = new HashSet<>();
    Set<String> apis = new HashSet<>();
    Set<String> rolekey = new HashSet<>();
    for (Role role : roles) {
      rolekey.add(role.getKey());
      Set<Permission> permissions = role.getPermissions();
      for (Permission permission : permissions) {
        String code = permission.getCode();
        switch (permission.getType()) {
          case 1:
            menus.add(code);
            break;
          case 2:
            points.add(code);
            break;
          case 3:
            apis.add(code);
            break;
          default:
            break;
        }
      }
    }
    this.roles.put("menus", menus);
    this.roles.put("points", points);
    this.roles.put("apis", apis);
    this.roles.put("rolekey", rolekey);
  }
}
