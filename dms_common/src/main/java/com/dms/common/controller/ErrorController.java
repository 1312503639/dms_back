package com.dms.common.controller;

import com.dms.common.entity.Result;
import com.dms.common.entity.ResultCode;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** @author 25377 */
@RestController
@CrossOrigin
public class ErrorController {

  /**
   * 公共错误跳转
   *
   * @param code
   * @return
   */
  @RequestMapping(value = "autherror")
  public Result autherror(int code) {
    return code == 1 ? new Result(ResultCode.UNAUTHENTICATED) : new Result(ResultCode.UNAUTHORISE);
  }
}
