package com.dms.common.utils;

import com.dms.common.entity.jwt.JwtConstants;
import com.dms.common.entity.jwt.JwtInfo;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;

import java.security.Key;

/**
 * @author Mr.Lgg
 * @date 2020/3/17 14:51
 */
public class JwtTokenUtils {
  private static Key getKeyInstance() {
    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    byte[] bytes = DatatypeConverter.parseBase64Binary("mall-user");
    return new SecretKeySpec(bytes, signatureAlgorithm.getJcaName());
  }

  /**
   * 生成token的方法
   *
   * @param jwtInfo
   * @param expire
   * @return
   */
  public static String generatorToken(JwtInfo jwtInfo, int expire) {
    return Jwts.builder()
        .setId(jwtInfo.getUid())
        .claim(JwtConstants.JWT_KEY_USER_ID, jwtInfo.getUid())
        .setExpiration(DateTime.now().plusSeconds(expire).toDate())
        .signWith(SignatureAlgorithm.HS256, getKeyInstance())
        .compact();
  }

  /**
   * 根据token获取token中的信息
   *
   * @param token
   * @return
   */
  public static JwtInfo getTokenInfo(String token) {

    Jws<Claims> claimsJws = Jwts.parser().setSigningKey(getKeyInstance()).parseClaimsJws(token);

    Claims claims = claimsJws.getBody();
    return new JwtInfo(claims.get(JwtConstants.JWT_KEY_USER_ID).toString());
  }
}
