package com.dms.common.exception;

import com.dms.common.entity.ResultCode;
import lombok.Getter;

/**
 * 自定义异常
 *
 * @author 25377
 */
@Getter
public class CommonException extends Exception {

  private ResultCode resultCode;

  public CommonException(ResultCode resultCode) {
    this.resultCode = resultCode;
  }
}
