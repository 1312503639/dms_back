package com.dms.common.entity.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mr.Lgg
 * @date 2020/3/17 14:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtInfo {
    private String uid;
}
